<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain otherpage">
    <img src="img/teamimg.jpg" alt="" class="img-fluid">
    <!-- container -->
    <div class="container">

        <!-- title row -->
        <div class="row ">
            <!-- col -->
            <div class="col-md-12">
                <div class="article pb-3 d-flex justify-content-between">                        
                    <h2 class="h4 pb-4 text-uppercase fgreen">Our Story</h2> 
                </div>
            </div>
            <!-- col -->        
        </div>
        <!-- title row -->

        <!-- row -->
        <div class="row pb-3">
            <!-- col -->
            <div class="col-md-4">
                <div class="graybox">
                    <h3 class="h4 text-uppercase">Vision</h3>
                    <p>Tro make self-sustainable ecosystem possible with out products</p>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-md-4">
                <div class="graybox">
                    <h3 class="h4 text-uppercase">Mission</h3>
                    <p>Tro make self-sustainable ecosystem possible with out products</p>
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-md-4">
                <div class="graybox">
                    <h3 class="h4 text-uppercase">Culture</h3>
                    <p>Tro make self-sustainable ecosystem possible with out products</p>
                </div>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->  

        <!-- journey -->
        <section class="about-journey">
          <!-- title row -->
          <div class="row ">
              <!-- col -->
              <div class="col-md-12 text-center">
                  <div class="article pb-3">                        
                      <h2 class="h4 pb-4 text-uppercase fgreen text-center">Our Journey</h2> 
                  </div>
              </div>
              <!-- col -->        
          </div>
          <!-- title row -->

          <!-- container -->
          <div class="container">
            <!-- row -->
            <div class="row">
              <!-- col -->
              <div class="col-12">
                <!-- card -->
                <div class="card">
                  <!-- card body -->
                  <div class="card-body">        
                
                      <!-- time line -->
                      <div class="timeline">
                        <!--1-->
                        <div class="timeline-wrapper timeline-wrapper-warning">
                          <div class="timeline-badge"></div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h6 class="timeline-title">2011</h6>
                            </div>
                            <div class="timeline-body">
                              <p>Idea - EV Two-Wheeler and EV ATV for India</p>
                            </div>                          
                          </div>
                        </div>
                        <!--/ 1-->

                          <!--2-->
                        <div class="timeline-wrapper timeline-inverted timeline-wrapper-danger">
                          <div class="timeline-badge"></div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h6 class="timeline-title">2016</h6>
                            </div>
                            <div class="timeline-body">
                              <p>Market Research</p>
                            </div>                           
                          </div>
                        </div>
                        <!--/2-->

                          <!--3-->
                        <div class="timeline-wrapper timeline-wrapper-success">
                          <div class="timeline-badge"></div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h6 class="timeline-title">2017</h6>
                            </div>
                            <div class="timeline-body">
                              <p>Team Building</p>

                              <ul>
                                 <li>Idea Validation on a bicycle with Li-ion battery pack version-1</li>
                                 <li>Li-ion Battery Pack Version-2 with BMS version-1 (Battery Management System)</li>
                                 <li>Li-ion Battery Pack Version-3 with BMS Version-2</li>
                                 <li>LFP Battery Pack Version 1 with BMS version-1</li>
                                 <li>Work-like Prototype with LFP Battery Pack Version - (Custom Chassis) </li>
                                 <li>Market validation (UAT) 2018 Look-like prototype with Middrive</li>
                              </ul>
                            </div>
                            
                          </div>
                        </div>
                        <!--3-->

                          <!--4-->
                        <div class="timeline-wrapper timeline-inverted timeline-wrapper-info">
                          <div class="timeline-badge"></div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h6 class="timeline-title">2018</h6>
                            </div>
                            <div class="timeline-body">
                            <ul>
                                 <li>Look-like prototype with Middrive Chassis - Li-ion Battery Pack Version 3 with Motor
                                    version 1 and MMS-1 (Motor management system)</li>
                                 <li>Look-like prototype with Hubdrive motor version-1 with Trellis cage Chassis - Li-ion Battery Pack Version 2- 3D Printed Vehicle Body</li>
                                 <li>GRAVTON QUANTA All-terrain Prototype 1 - Green Vehicle</li>
                                 <li>Market Validation (UAT)</li>                                
                              </ul>
                            </div>
                           
                          </div>
                        </div>
                        <!--/4-->

                        <!--5-->
                        <div class="timeline-wrapper timeline-wrapper-primary">
                          <div class="timeline-badge"></div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h6 class="timeline-title">2019</h6>
                            </div>
                            <div class="timeline-body">
                                <ul>
                                  <li>Company Registration</li>
                                  <li>Startup India Recognition</li>
                                  <li>Key Collaborations</li>
                                  <li>Design Patents 9</li>
                                  <li>Trademark 1</li>
                                  <li>GRAVTON QUANTA X Prototype version1 - Red Vehicle</li>
                                  <li>GRAVTON QUANTA S Prototype version 1 - Black Vehicle</li>
                                  <li>GRAVTON QUARK Prototype version-1 - ICAT Vehicle</li>
                                </ul>                                
                            </div>
                           
                          </div>
                        </div>
                        <!--/5-->

                          <!--6-->
                        <div class="timeline-wrapper timeline-inverted timeline-wrapper-info">
                          <div class="timeline-badge"></div>
                          <div class="timeline-panel">
                            <div class="timeline-heading">
                              <h6 class="timeline-title">2020</h6>
                            </div>
                            <div class="timeline-body">
                              <ul>
                                  <li>GOTAC version-1</li>
                                  <li>GRAVTON QUANTA S Prototype version-2 - Blue Vehicle</li>
                                  <li>Mobile Application version-1</li>
                                  <li>First Long Distance Field Testing 300kms with single swap</li>
                                  <li>WMI Code Allocation</li>
                                  <li>ICAT Approval</li>                                 
                                </ul>       
                            </div>
                            
                          </div>
                        </div>
                        <!--/6-->                        
                      </div>
                      <!--/ time line -->                  
                  </div>
                  <!--/ card body -->
                </div>
                <!-- casrd/ -->
              </div>
              <!--/ col -->
            </div>
            <!--/ row-->
           </div>
        <!--/ container -->
        </section>
        <!--/ journey -->
    </div>
    <!--/ container -->

    <!-- philosphy -->
    <section class="philosphy">

    <div class="container">

        <!-- title row -->
        <div class="row ">
              <!-- col -->
              <div class="col-md-12 text-center">
                  <div class="article pb-3">                        
                      <h2 class="h2 pb-4 text-uppercase text-center">Philosphy</h2> 
                  </div>
              </div>
              <!-- col -->        
          </div>
          <!-- title row -->

          <!--row -->
          <div class="row py-3">
              <!-- col -->
              <div class="col-md-6">
                  <img src="img/philosphy01.jpg" alt="" class="img-fluid">
              </div>
              <!--/ col -->

              <!-- col -->
              <div class="col-md-6">
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt numquam aperiam consectetur magnam quam facere illum culpa. Et facere voluptatum maiores atque cum! Quibusdam ad officia, praesentium, eos illum ut repellat inventore facilis, necessitatibus iusto doloribus at eveniet magni. Dignissimos, asperiores dolore? Cupiditate amet quas officiis temporibus corporis aliquam explicabo, dolorem odio hic deleniti? Quae consectetur sed officia aperiam quia quo quasi explicabo? Perferendis, dolores voluptas? Laborum repudiandae similique fuga consectetur, beatae perspiciatis eveniet a! Aperiam reprehenderit mollitia velit neque laborum. Dolores tenetur distinctio cum adipisci doloribus eaque, dolore natus deserunt ullam quae pariatur, facilis amet hic voluptates animi, laboriosam soluta vel voluptas consectetur alias? Blanditiis voluptatem reiciendis nam unde similique, pariatur sequi iusto dolores! Amet eius iure magni, minima neque saepe molestiae quaerat illum doloremque facilis voluptate esse sit consequuntur nulla earum sequi soluta expedita inventore tempora repellat quae incidunt. </p>
              </div>
              <!--/ col -->
          </div>
          <!--/ row -->

           <!--row -->
           <div class="row py-3">
              <!-- col -->
              <div class="col-md-6 order-lg-last">
                  <img src="img/philosphy02.jpg" alt="" class="img-fluid">
              </div>
              <!--/ col -->

              <!-- col -->
              <div class="col-md-6">
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt numquam aperiam consectetur magnam quam facere illum culpa. Et facere voluptatum maiores atque cum! Quibusdam ad officia, praesentium, eos illum ut repellat inventore facilis, necessitatibus iusto doloribus at eveniet magni. Dignissimos, asperiores dolore? Cupiditate amet quas officiis temporibus corporis aliquam explicabo, dolorem odio hic deleniti? Quae consectetur sed officia aperiam quia quo quasi explicabo? Perferendis, dolores voluptas? Laborum repudiandae similique fuga consectetur, beatae perspiciatis eveniet a! Aperiam reprehenderit mollitia velit neque laborum. Dolores tenetur distinctio cum adipisci doloribus eaque, dolore natus deserunt ullam quae pariatur, facilis amet hic voluptates animi, laboriosam soluta vel voluptas consectetur alias? Blanditiis voluptatem reiciendis nam unde similique, pariatur sequi iusto dolores! Amet eius iure magni, minima neque saepe molestiae quaerat illum doloremque facilis voluptate esse sit consequuntur nulla earum sequi soluta expedita inventore tempora repellat quae incidunt. </p>
              </div>
              <!--/ col -->
          </div>
          <!--/ row -->

           <!--row -->
           <div class="row py-3">
              <!-- col -->
              <div class="col-md-6">
                  <img src="img/philosphy03.jpg" alt="" class="img-fluid">
              </div>
              <!--/ col -->

              <!-- col -->
              <div class="col-md-6">
                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt numquam aperiam consectetur magnam quam facere illum culpa. Et facere voluptatum maiores atque cum! Quibusdam ad officia, praesentium, eos illum ut repellat inventore facilis, necessitatibus iusto doloribus at eveniet magni. Dignissimos, asperiores dolore? Cupiditate amet quas officiis temporibus corporis aliquam explicabo, dolorem odio hic deleniti? Quae consectetur sed officia aperiam quia quo quasi explicabo? Perferendis, dolores voluptas? Laborum repudiandae similique fuga consectetur, beatae perspiciatis eveniet a! Aperiam reprehenderit mollitia velit neque laborum. Dolores tenetur distinctio cum adipisci doloribus eaque, dolore natus deserunt ullam quae pariatur, facilis amet hic voluptates animi, laboriosam soluta vel voluptas consectetur alias? Blanditiis voluptatem reiciendis nam unde similique, pariatur sequi iusto dolores! Amet eius iure magni, minima neque saepe molestiae quaerat illum doloremque facilis voluptate esse sit consequuntur nulla earum sequi soluta expedita inventore tempora repellat quae incidunt. </p>
              </div>
              <!--/ col -->
          </div>
          <!--/ row -->

      </div>
      <!--/ container -->



    </section>
    <!--/ philosphy -->
    </section>
    <!--/ specs -->

    <!--/ tab -->

    

    
       
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
