<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain">
    <!-- container -->
    <div class="container">

    <!-- title row -->
    <div class="row ">
        <!-- col -->
        <div class="col-md-12">
            <div class="article pb-3 d-flex justify-content-between blogdetail_title">                        
                <h2 class="h4 pb-4 text-uppercase fgreen">Blog / News</h2>                    
                <a href="bloglist.php" class="greentext"><span class="icon-long-arrow-left"></span> Back to Blogs</a>
            </div>
        </div>
        <!-- col -->        
    </div>
    <!-- title row -->

    <!-- row -->
    <div class="row justify-content-center blogdetail-col">
        <div class="col-md-8">

            <h4 class="py-3">All about the range of an EV, in flat 9 minutes.</h4>
            <p class="d-flex justify-content-between smp">
                <small>Posted on 25-09-2020</small>  
                <small>Posted by <b>Admin</b></small>  
            </p>

            <figure class="pb-3">
                <img src="img/blog/blog01.jpg" alt="" class="img-fluid">
            <figure>

            <article class="pt-3">
                <p>Since electric vehicles (EV) have hit the Indian market, the range has been like a three-ring circus. People often keep juggling between these major questions — What exactly does the range in an EV mean? How is the range calculated? And what are the factors that control range? It’s just a matter of understanding. And here’s a piece that will help you do that.</p>

                <p>We are not very far from the era of electric. Electric vehicles are slowly and steadily gaining momentum, in our part of the world. It’s understandable that every new thing takes its own sweet time to make its place. More importantly, get accepted and understood. And here’s a relatively new breed of vehicles built on an unconventional platform, powered by alternative fuel, and topped with new tech. Unlike mileage, which is associated with ICE vehicles, and has been around for over a century, the range in an electric vehicle is a new concept. But, not so difficult to understand, so let’s take a dive into it.</p>

                <h5 class="h5">Range, in the simplest words</h5>
                <p>The distance that a user can cover on a fully charged vehicle is called Range. Nothing more, nothing less. We can call it the ‘mileage’ equivalent in an electric vehicle. The phrase that we keep hearing “Kitna deti hai” is synchronous to range too. While mileage is more about fuel economy, the range is about the distance you can travel in one full charge, obviously because the cost efficiency is not a worry when it comes to electric.</p>

                <p>Yes, it is a very new concept, and as compared to over a century old mileage, that all of us are used to, it needs a much better understanding.</p>

                <p>Government bodies test and certify the vehicle (the process is called Homologation), based on standard city riding conditions, which has been defined as Indian Driving Cycle. Most of the auto OEMs communicate this number as the mileage of a particular vehicle.</p>

                <p>But, the point that needs attention is that the ideal conditions may be different from the real conditions. What’s the difference? Well, the road condition, traffic condition, and most importantly the riding behaviour influence the numbers. And these differ from person to person, right? And because of that different people get different mileage, even when they are riding the same vehicle.</p>

                <p>The range is no different.</p>

                <h5 class="h5">Range, in the simplest words</h5>
                <p>The distance that a user can cover on a fully charged vehicle is called Range. Nothing more, nothing less. We can call it the ‘mileage’ equivalent in an electric vehicle. The phrase that we keep hearing “Kitna deti hai” is synchronous to range too. While mileage is more about fuel economy, the range is about the distance you can travel in one full charge, obviously because the cost efficiency is not a worry when it comes to electric.</p>
            </article>


        </div>
    </div>
    <!--/ row -->

    
       
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
