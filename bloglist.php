<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain">
    <!-- container -->
    <div class="container">

    <!-- title row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-4">
            <div class="article pb-3">                        
                <h2 class="h4 pb-4 text-uppercase fgreen">Blog / News</h2>                    
            </div>
        </div>
        <!-- col -->        
    </div>
    <!-- title row -->

    <!-- row -->
    <div class="row blogrow">
        <!-- col -->
        <div class="col-md-6">
            <a href="blogDetail.php">
                <img src="img/blog/blog01.jpg" alt="" class="img-fluid">
            </a>
        </div>
        <!--/ col -->
        <!-- col -->
        <div class="col-md-6 blogmaincol">
            <div class="blog-col">              
                <article>
                    <div class="d-fles">                        
                        <h5 class="h5 blogby">
                            Autocar 
                        </h5>
                    </div>
                    <h4>
                        <a href="blogDetail.php">Ultraviolette electric bike prototyperide</a>                        
                    </h4>
                    <p>Bengaluru-based start-up Ultraviolette wants to outperform 200-250cc motorcycles with its upcoming EV. Having ridden the final prototype, we can confirm that they’re on track to do just that.</p>
                    <p class="d-flex justify-content-between">
                        <small>Posted on 25-09-2020</small>  
                        <small>Posted by <b>Admin</b></small>  
                    </p>
                   <p class="text-right pb-0">
                        <a href="blogDetail.php" class="bloglink">Read More <span class="icon-long-arrow-right icomoon position-relative" style="top:2px"></span> </a>
                   </p>
                </article>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->

      <!-- row -->
      <div class="row blogrow">
        <!-- col -->
        <div class="col-md-6">
                <a href="javascript:void(0)">
                    <img src="img/blog/blog02.jpg" alt="" class="img-fluid">
                </a>
            </div>
            <!--/ col -->

          <!-- col -->
          <div class="col-md-6 blogmaincol">
            <div class="blog-col">              
                <article>
                    <div class="d-fles">                        
                        <h5 class="h5 blogby">
                            Live Mint
                        </h5>
                    </div>
                    <h4>
                        <a href="javascript:void(0)">Revolt e-motorcycle gets ARAI range...</a>                        
                    </h4>
                    <p>Bengaluru-based start-up Ultraviolette wants to outperform 200-250cc motorcycles with its upcoming EV. Having ridden the final prototype, we can confirm that they’re on track to do just that.</p>
                    <p class="d-flex justify-content-between">
                        <small>Posted on 25-09-2020</small>  
                        <small>Posted by <b>Admin</b></small>  
                    </p>
                    <p class="text-right">
                        <a href="javascript:void(0)" class="bloglink">Read More <span class="icon-long-arrow-right icomoon position-relative" style="top:2px"></span> </a>
                   </p>
                </article>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->

    <!-- row -->
    <div class="row blogrow">
        <!-- col -->
        <div class="col-md-6">
                <a href="javascript:void(0)">
                    <img src="img/blog/blog04.jpg" alt="" class="img-fluid">
                </a>
            </div>
            <!--/ col -->
          <!-- col -->
          <div class="col-md-6 blogmaincol">
            <div class="blog-col">              
                <article>
                    <div class="d-fles">                        
                        <h5 class="h5 blogby">
                            Business Standard
                        </h5>
                    </div>
                    <h4>
                        <a href="javascript:void(0)">Micromax's Sharma Gears up to Jump...</a>                        
                    </h4>
                    <p>Bengaluru-based start-up Ultraviolette wants to outperform 200-250cc motorcycles with its upcoming EV. Having ridden the final prototype, we can confirm that they’re on track to do just that.</p>
                    <p class="d-flex justify-content-between">
                        <small>Posted on 25-09-2020</small>  
                        <small>Posted by <b>Admin</b></small>  
                    </p>
                    <p class="text-right">
                        <a href="javascript:void(0)" class="bloglink">Read More <span class="icon-long-arrow-right icomoon position-relative" style="top:2px"></span> </a>
                   </p>
                </article>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->

    <!-- row -->
    <div class="row blogrow">
        <!-- col -->
        <div class="col-md-6">
                <a href="javascript:void(0)">
                    <img src="img/blog/blog05.jpg" alt="" class="img-fluid">
                </a>
            </div>
            <!--/ col -->

          <!-- col -->
          <div class="col-md-6 blogmaincol">
            <div class="blog-col">              
                <article>
                    <div class="d-fles">                        
                        <h5 class="h5 blogby">
                            Bloomberg Quint 
                        </h5>
                    </div>
                    <h4>
                        <a href="javascript:void(0)">With Al-enabled electric vehicle...</a>                        
                    </h4>
                    <p>Bengaluru-based start-up Ultraviolette wants to outperform 200-250cc motorcycles with its upcoming EV. Having ridden the final prototype, we can confirm that they’re on track to do just that.</p>
                    <p class="d-flex justify-content-between">
                        <small>Posted on 25-09-2020</small>  
                        <small>Posted by <b>Admin</b></small>  
                    </p>
                    <p class="text-right">
                        <a href="javascript:void(0)" class="bloglink">Read More <span class="icon-long-arrow-right icomoon position-relative" style="top:2px"></span> </a>
                   </p>
                </article>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->


    <!-- row -->
    <div class="row blogrow">
        <!-- col -->
        <div class="col-md-6">
                <a href="javascript:void(0)">
                    <img src="img/blog/blog06.jpg" alt="" class="img-fluid">
                </a>
            </div>
            <!--/ col -->
          <!-- col -->
          <div class="col-md-6 blogmaincol">
            <div class="blog-col">              
                <article>
                    <div class="d-fles">                        
                        <h5 class="h5 blogby">
                            Inc42 
                        </h5>
                    </div>
                    <h4>
                        <a href="javascript:void(0)">Micromax co-founder to make Al-based...</a>                        
                    </h4>
                    <p>Bengaluru-based start-up Ultraviolette wants to outperform 200-250cc motorcycles with its upcoming EV. Having ridden the final prototype, we can confirm that they’re on track to do just that.</p>
                    <p class="d-flex justify-content-between">
                        <small>Posted on 25-09-2020</small>  
                        <small>Posted by <b>Admin</b></small>  
                    </p>
                    <p class="text-right">
                        <a href="javascript:void(0)" class="bloglink">Read More <span class="icon-long-arrow-right icomoon position-relative" style="top:2px"></span> </a>
                   </p>
                </article>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->

    <!-- row -->
    <div class="row blogrow">
        <!-- col -->
        <div class="col-md-6">
                <a href="javascript:void(0)">
                    <img src="img/blog/blog03.jpg" alt="" class="img-fluid">
                </a>
            </div>
            <!--/ col -->

          <!-- col -->
          <div class="col-md-6 blogmaincol">
            <div class="blog-col">              
                <article>
                    <div class="d-fles">                        
                        <h5 class="h5 blogby">
                            Autocar 
                        </h5>
                    </div>
                    <h4>
                        <a href="javascript:void(0)">Micromax co-founder Rahul Sharma ...</a>                        
                    </h4>
                    <p>Bengaluru-based start-up Ultraviolette wants to outperform 200-250cc motorcycles with its upcoming EV. Having ridden the final prototype, we can confirm that they’re on track to do just that.</p>
                    <p class="d-flex justify-content-between">
                        <small>Posted on 25-09-2020</small>  
                        <small>Posted by <b>Admin</b></small>  
                    </p>
                    <p class="text-right">
                        <a href="javascript:void(0)" class="bloglink">Read More <span class="icon-long-arrow-right icomoon position-relative" style="top:2px"></span> </a>
                   </p>
                </article>
            </div>
        </div>
        <!--/ col -->
    </div>
    <!--/ row -->

    <!-- row -->
    <div class="row justify-content-center py-4">
        <div class="col-md-12 text-center page-navigation">           
            <a href="javascript:void(0)">1</a>
            <a href="javascript:void(0)">2</a>
            <a href="javascript:void(0)">3</a>
            <a href="javascript:void(0)">4</a>           
        </div>
    </div>
    <!--/ row -->
       
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
