<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>
<!-- main -->
<main class="subMain booknowpage">
    <!-- container fluid -->
    <div class="cust-container">
        <div class="row ">
            <div class="col-md-12">
                <h5 class="h5 bookRowtitle">Choose Your Desired Vehicle and colour</h5>
            </div>
        </div>
        <div class="row py-3 px-0">
            <div class="col-md-4 parentVehicleCol">
                <div class="vehicleCol">
                   <label class="custom-checkbox">
                        <input type="hidden" name="alarm" value="False" />
                        <input class="custom-checkbox-input" name="alarm" value="True" type="radio">
                        <span class="custom-checkbox-text"><span class="icon-check icomoon"></span></span>
                    </label>
                   <a href="javascript:void(0)">
                        <h5>Quanta X Black Colour</h5>
                        <img src="img/vehImages/qx-color-black.png" alt="" class="img-fluid vehImage">
                   </a>
                </div>
            </div>
            <div class="col-md-4 parentVehicleCol">
                <div class="vehicleCol">
                    <label class="custom-checkbox">
                        <input type="hidden" name="alarm" value="False" />
                        <input class="custom-checkbox-input" name="alarm" value="True" type="radio">
                        <span class="custom-checkbox-text"><span class="icon-check icomoon"></span></span>
                    </label>
                    <a href="javascript:void(0)">
                         <h5>Quanta X White Colour</h5>
                        <img src="img/vehImages/qx-color-gray.png" alt="" class="img-fluid vehImage">
                    </a>
                </div>
            </div>
            <div class="col-md-4 parentVehicleCol">
                <div class="vehicleCol">
                    <label class="custom-checkbox">
                        <input type="hidden" name="alarm" value="False" />
                        <input class="custom-checkbox-input" name="alarm" value="True" type="radio">
                        <span class="custom-checkbox-text"><span class="icon-check icomoon"></span></span>
                    </label>
                    <a href="javascript:void(0)">
                         <h5>Quanta X Red Colour</h5>
                        <img src="img/vehImages/qx-color-red.png" alt="" class="img-fluid vehImage">
                    </a>
                </div>
            </div>
        </div>

        <!-- form section -->
        <div class="row justify-content-center">
            <div class="col-md-5">
                <!-- title -->
                <div class="row ">
                    <div class="col-md-12">
                        <h5 class="h5 bookRowtitle">Quanta X White Colour</h5>
                    </div>
                </div>
                <!-- / title -->
                <!-- form -->
                <div class="form">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Select State</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Select State</option>
                                        <option>Telangana</option>
                                        <option>Andhra Pradesh</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>City</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Select City</option>
                                        <option>Hyderabad</option>
                                        <option>Secunderabad</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Experience Centers</label>
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>Select Experience centers</option>
                                        <option>Hyderabad Motors</option>
                                        <option>Secunderabad Motors</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <!-- title -->
                        <h5 class="h5 bookRowtitle">Contact Details</h5>
                            <!-- / title -->
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Email Address</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter your Email Address">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Confirm Email Address</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter your Email Address">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter your First Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter your Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Enter Your Phone Number">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- title -->
                    <div class="row ">
                        <div class="col-md-12">
                            <h5 class="h5 bookRowtitle">Billing Address</h5>
                        </div>
                    </div>
                    <!-- / title -->
                    <div class="row">
                        <div class="col-md-12">
                                <div class="form-group">
                                    <label>Addrerss</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Enter your Door No, Floor, building name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Landmark</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Near Mall, park, School etc">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>PIN Code</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="6 Digit PIN Code">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>City</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="City" disabled>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <!--/ form -->


            </div>
        </div>
        <!--/ form section -->

       
        
        <div class="row justify-content-center">
            <div class="col-md-5">
                <h5 class="h5 bookRowtitle text-center">Booking Summary</h5>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 mx-auto summaryDiv">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row  py-3 border-bottom justify-content-between">
                            <div class="col-md-6"><h6>Quanta X White Colour</h6></div>
                            <div class="col-md-6 text-right"><span>Rs:2,500.00 (<small>Fully Refunded</small>)</span></div>   
                        </div>                      
                        <p>
                            <small>You can select your preferred ownership model and mode of payment for your  Quanta X White at the time of purchase</small>
                        </p>
                        <p>
                            <input type="checkbox"> I agree to the <a class="fgreen" href="javascript:void(0)">Terms &amp; Conditions</a>
                        </p>
                        <button class="w-100 blackbrdBtn mt-4">Proceed and Pay</button>
                        <small>Trouble booking? Reach out to us at +91 7676 600 900</small>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!--/ container fluid -->
</main>

<!--/ main -->
<?php 
    include 'includes/footer.php';
?>
<?php 
    include 'includes/scripts.php';
?>

</body>
</html>
