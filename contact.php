<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain contact">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!--/ col -->
            <div class="col-md-4">
                <div class="article pb-3">                        
                    <h2 class="h4 pb-4 text-uppercase fgreen">Contact</h2>
                </div>
                <div class="address">
                    <h4>Email us</h4>
                    <p>
                        <a href="mailto:support@gravtonmotors.com">support@gravtonmotors.com</a>
                    </p>                    
                </div>

                <div class="address">
                    <h4>Call us</h4>
                    <p>
                        <a href="tel:9642123254">+91 9642123254</a>
                    </p>
                    <p>Helpline (Mon-Sun, 10AM - 7PM):</p>
                </div>

                <div class="address">
                    <h4>Corporate Office</h4>                   
                    <p>Plot No:91, Industrial Area, Nacharam IDA, Near Tarnaka, Hyderabad - 73</p>
                </div>
            </div>
            <!--/ col -->
              <!--/ col -->
              <div class="col-md-8">
                <div class="article pb-3">                        
                        <h2 class="h4 pb-4 text-uppercase fgreen">Reach us</h2>
                    
                        <!-- form -->
                        <form class="form" method="">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Write Your Name</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Write Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                
                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Write Phone Number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Phone Number">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Subject">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <div class="input-group">
                                           <textarea class="form-control" style="height:100px;" placeholder="Write your Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->                            
                            <input type="submit" class="btn greenBtn w-100" value="Submit">
           
                        </form>
                        <!--/ form -->
                    </div>
              </div>
            <!--/ col -->
        </div>
        <!--/ row -->
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
