<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    
</head>

<body class="sb-nav-fixed user-screen">

<!-- main -->
<div id="layoutSidenav">
        <?php
            include 'includes/headerPostlogin.php';
        ?>

        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-flex justify-content-between pb-2 border-bottom userRightTitle">
                    <h3 class="mt-2 fbold h4 mb-0 pb-0">New Pre Orders</h3>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a href="userProfile.php">Praveen Kumar</a></li>
                        <li class="breadcrumb-item active"><a href="userPreOrders.php">My Pre Orders</a></li>
                        <li class="breadcrumb-item active">New Pre Order</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">  
                        <!-- row -->
                        <div class="row pt-5">
                            <!-- col -->
                            <div class="col-md-12">
                            <p class="text-right pb-3">
                                <a href="userPreOrders.php" class="greenBtn">Back to My Preorders</a>
                            </p>

                            <!-- stepper -->
                            <div class="customStepper">
                            <!-- content -->
                            <div class="content">
                                <!--content inner-->
                                <div class="content__inner">   
                                    <div class="container-fluid overflow-hidden">
                                    <!--multisteps-form-->
                                    <div class="multisteps-form">
                                        <!--progress bar-->
                                        <div class="row">
                                            <div class="col-12 col-lg-10 ml-auto mr-auto mb-4">
                                                <div class="multisteps-form__progress">
                                                    <button class="multisteps-form__progress-btn js-active" type="button" title="User Terms">Terms</button>
                                                    <button class="multisteps-form__progress-btn" type="button" title="User Info">User Info</button>
                                                    <button class="multisteps-form__progress-btn" type="button" title="Address">Address</button>
                                                    <button class="multisteps-form__progress-btn" type="button" title="Order Info">Pre Order Info</button>                                                   
                                                    <button class="multisteps-form__progress-btn" type="button" title="Comments">Payment</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--form panels-->
                                        <div class="row">
                                        <div class="col-12 col-lg-10 m-auto">
                                            <form class="multisteps-form__form">

                                             <!--Step 00-->
                                             <div class="multisteps-form__panel p-1 p-sm-4 rounded bg-white js-active">
                                                
                                                <h3 class="multisteps-form__title">User Terms</h3>
                                                <div class="div_content">    
                                                    <ul class="list-items">
                                                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa, aliquam?</li>
                                                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                                                        <li>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Omnis corporis architecto saepe, repudiandae vitae harum!</li>
                                                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa, aliquam?</li>
                                                        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
                                                        <li>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Omnis corporis architecto saepe, repudiandae vitae harum!</li>
                                                    </ul>
                                                    <div class="col-12">
                                                        <div class="form-check pt-3">
                                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                            <label class="form-check-label" for="exampleCheck1">I Agree with Terms & Conditions</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="button-row d-flex mt-4">
                                                    <button class="btn greenBtn ml-auto js-btn-next" type="button" title="Next">Proceed</button>
                                                </div>
                                            </div>
                                            <!--Step 00-->

                                            <!--Step 01-->
                                            <div class="multisteps-form__panel p-1 p-sm-4 rounded bg-white">
                                                <h3 class="multisteps-form__title">Your Info</h3>
                                                <div class="multisteps-form__content">
                                                    <div class="form-row mt-4 customForm form-group">
                                                        <div class="col-12 col-sm-6">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="Praveen" value="Praveen"/>
                                                        </div>
                                                        <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="Kumar"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4 customForm form-group">
                                                        <div class="col-12 col-sm-6">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="mail" value="praveennandipati@gmail.com"/>
                                                        </div>
                                                        <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                        <input class="multisteps-form__input form-control" type="email" placeholder="Phone Number" value="9642123254"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4 customForm form-group">
                                                        <div class="col-12 col-sm-6">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="Alternative Phone Number" />
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="row">
                                                    <div class="button-row d-flex mt-4 col-12">
                                                    <button class="btn greenBtn js-btn-prev" type="button" title="Prev">Prev</button>
                                                    <button class="btn greenBtn ml-auto js-btn-next" type="button" title="Next">Next</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Step 01-->

                                            <!--Step 02-->
                                            <div class="multisteps-form__panel p-1 p-sm-4 rounded bg-white">
                                                <h3 class="multisteps-form__title">Your Address</h3>
                                                <div class="multisteps-form__content">
                                                    <div class="form-row mt-4 customForm form-group">
                                                        <div class="col">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="Address 1"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4 customForm form-group">
                                                        <div class="col">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="Address 2"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-row mt-4 customForm form-group">
                                                        <div class="col-12 col-sm-6">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="City"/>
                                                        </div>
                                                        <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                                                            <input list="states" name="" id="" class="multisteps-form__input form-control" placeholder="state">
                                                            <datalist id="states">
                                                                <option value="Telangana">
                                                                <option value="Andhra Pradesh">
                                                                <option value="Tamilnadu">
                                                                <option value="Kerala">
                                                                <option value="Maharastra">
                                                            </datalist>
                                                        </div>
                                                        <div class="col-6 col-sm-3 mt-4 mt-sm-0">
                                                        <input class="multisteps-form__input form-control" type="text" placeholder="Zip"/>
                                                        </div>
                                                        <div class="col-12">
                                                            <div class="form-check pt-3">
                                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                                <label class="form-check-label" for="exampleCheck1">Add this address to Address Manager</label>
                                                            </div>
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                <div class="button-row d-flex mt-4 customForm form-group">
                                                    <button class="btn greenBtn js-btn-prev" type="button" title="Prev">Prev</button>
                                                    <button class="btn greenBtn ml-auto js-btn-next" type="button" title="Next">Next</button>
                                                </div>
                                            </div>
                                            <!--Step 02-->

                                            <!--Step 03-->
                                            <div class="multisteps-form__panel p-1 p-sm-4 rounded bg-white">
                                                <h3 class="multisteps-form__title">Pre Order Info</h3>
                                                <div class="multisteps-form__content">
                                                    <div class="row">
                                                        <div class="col-12 col-md-6 mt-4">
                                                            <div class="card shadow-sm p-1 p-sm-4">
                                                                <div class="form-row mt-4 customForm form-group">
                                                                    
                                                                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                                        <input list="models" name="" id="" class="multisteps-form__input form-control" placeholder="Select Category">
                                                                        <datalist id="models">
                                                                            <option value="Two Wheelers">
                                                                            <option value="Other">
                                                                        </datalist>
                                                                    </div>
                                                                    <div class="col-12 col-sm-6 mt-4 mt-sm-0">
                                                                        <input list="veh" name="" id="" class="multisteps-form__input form-control" placeholder="Select Item">
                                                                        <datalist id="veh">
                                                                            <option value="Quanta X">
                                                                            <option value="Quanta S">
                                                                        </datalist>
                                                                    </div>
                                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6 mt-4">
                                                        <div class="card shadow-sm">
                                                            <div class="card-body">  
                                                            <p class="card-text pb-2">You Have Selected <b>Quanta X </b> <a href="javascript:void(0)">Read More about this vehicle</a>  </p>
                                                            <p class="card-text pb-2">Your Pre Booked Bike Available Estimated Date is <b>6 Oct 2020 at Showroom, Kukatpally </b>   </p>
                                                            <p class="card-text">We Will Intimate You soon to your registered Mobile Number</p>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                <div class="row">
                                                    <div class="button-row d-flex mt-4 col-12">
                                                    <button class="btn greenBtn js-btn-prev" type="button" title="Prev">Prev</button>
                                                    <button class="btn greenBtn ml-auto js-btn-next" type="button" title="Next">Next</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Step 03-->
                                           

                                            <!--Step 04-->
                                            <div class="multisteps-form__panel p-4 rounded bg-white">
                                                <h3 class="multisteps-form__title text-center">Payment</h3>
                                                <div class="multisteps-form__content">
                                                    <div class="form-row mt-4 customForm form-group justify-content-center">
                                                        <div class="col-12 col-md-6 mt-4">
                                                            <div class="card shadow-sm">
                                                                <div class="card-body text-center">     
                                                                <p class="card-text">Token Amount</p>
                                                                    <h3 class="h3 fbold fgreen">Rs:5,000</h3>                                                  
                                                                    <p class="card-text pb-2">This Token Amount will deduct at the time of Full Payment of purchase time  </p>
                                                                    <p class="card-text pb-2">Booking Details will be updated to <b>+91 9642123254 </b>   </p>
                                                                    <button onclick="window.location.href='userPreOrders.php';" class="btn greenBtn" type="submit" title="Send">Pay Now</button>
                                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>
                                                <div class="button-row d-flex mt-4">
                                                    <button class="btn greenBtn js-btn-prev" type="button" title="Prev">Prev</button>
                                                </div>
                                            </div>
                                            <!--Step 04-->
                                            </form>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                <!--/ content -->


                            </div>
                            <!--/ stepper -->                               

                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ rw -->                     
                    </div>
                    <!--/ container fluid -->                    

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->
<?php 
    include 'includes/scripts.php';
?>

    
</body>
</html>
