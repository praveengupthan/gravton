<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!--/ col -->
            <div class="col-md-5">
                <div class="article pb-3">                        
                    <h2 class="h4 pb-4 text-uppercase fgreen">The future of automobiles. The future of retail.</h2>
                    <p>
                    Quanta is the first step towards an electric and intelligent future. In a short span of time, it has become a benchmark for what modern scooters should be. But we didn’t just stop at building a great product, we went a step further and redefined the future of retail as well: thoughtful, educative, experiential and non-salesy.
                    </p>
                    <p>We have embarked on a long journey to make all commute electric and achieving this vision at an accelerated pace, no doubt, requires like-minded partners. As we expand our footprint across India, we look forward to partnering with business leaders who share our beliefs and are interested in investing in this future.</p>
                    <p>If this excites you, feel free to share your details in the form.</p>
                </div>
               

               
               
            </div>
            <!--/ col -->
              <!--/ col -->
              <div class="col-md-7">
                <div class="article pb-3">                        
                        <h2 class="h4 pb-4 text-uppercase fgreen">Reach us</h2>                    
                        <!-- form -->
                        <form class="text-uppercase" method="">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name of the Main Applicant</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Name">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Enter your preferred city</label>
                                        <div class="input-group">
                                        <input list="prefercity" name="" class="form-control">
                                            <datalist id="prefercity">
                                                <option value="Hyderabad">
                                                <option value="Bengaloru">
                                                <option value="Chennai">
                                                <option value="Mumbai">
                                                <option value="Tiruvananthapuram">
                                            </datalist>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                
                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Age of Applicant</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" placeholder="Age">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mobile Number</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Mobile">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Any existing Automobile Business</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Ex:Running 2 Automobile showrooms">
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <div class="input-group">
                                           <textarea class="form-control" style="height:100px;" placeholder="Write your Message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->                            
                            <input type="submit" class="btn greenBtn w-100" value="Submit">
           
                        </form>
                        <!--/ form -->
                    </div>
              </div>
            <!--/ col -->
        </div>
        <!--/ row -->
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
