<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain otherpage">
    <!-- container -->
    <div class="container">

        <!-- title row -->
        <div class="row ">
            <!-- col -->
            <div class="col-md-12">
                <div class="article pb-3 d-flex justify-content-between">                        
                    <h2 class="h4 pb-4 text-uppercase fgreen">Frequently asked Questions</h2>                    
                
                </div>
            </div>
            <!-- col -->        
        </div>
        <!-- title row -->
    

        <!-- tab -->
        <div class="specs-detail pt-0">
        <!-- spec tab -->       
            <div class="spectab pt-0">
                <!--Vertical Tab-->
                <div id="spectab">
                    <ul class="resp-tabs-list hor_1">
                        <li>General</li>
                        <li>Vehicle</li>
                        <li>Management</li>                                      
                    </ul>
                    <div class="resp-tabs-container hor_1">

                        <!-- Performance-->
                        <div>
                            <h5 class="fgreen">General</h5>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                        </div>
                        <!--/ Performance-->

                        <!-- Range-->
                        <div>
                        <h5 class="fgreen">Range</h5>
                        <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                        </div>
                        <!-- /Range-->

                        <!-- Charging-->
                        <div> 
                        <h5 class="fgreen">Management</h5>
                        <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                            <div class="qandans">
                                <h6>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, ex.  molestiae?</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro praesentium delectus aut, adipisci dolorem voluptatum. Perferendis eius quaerat at nemo, a tempora excepturi similique consectetur iusto vel earum aut omnis!</p>
                            </div>
                        </div>
                        <!-- /Charging-->                                           
                    </div>
                </div>
                <!--/ vertical tab -->
            </div>
            <!--/ spec tab -->
        </div>
        <!-- /tab -->
    </div>
    <!--/ container -->
    </section>
    <!--/ specs -->

    <!--/ tab -->

    

    
       
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
