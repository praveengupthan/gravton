
<!-- footer -->
<footer>
    <!-- container fluid -->
    <div class="container-fluid">
        <!-- row -->
        <div class="row justify-content-center">
            <!-- col -->
            <div class="col-md-4 text-center">
                <a class="footer-brand">
                    <img src="img/logo.png" alt="">
                </a>               
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-md-4 text-center">               
                <div class="footerlinks">
                    <a href="javascript:void(0)">About </a>
                    <a href="blog.php">Media</a>
                    <a href="faq.php">Faq</a>
                    <!-- <a href="javascript:void(0)">Careers</a> -->
                    <a href="dealer.php">Become Dealer</a>
                    <a href="terms.php">Terms</a>
                    <a href="privacy.php">Privacy</a>
                    <a href="contact.php">Contact</a>                    
                </div>
            </div>
            <!--/ col -->

             <!-- col -->
             <div class="col-md-4 text-center">               
                <div class="footerlinks sociallinks">
                   <a href="javascript:void(0)"><span class="icon-facebook icomoon"></span></a>
                    <a href="javascript:void(0)"><span class="icon-twitter icomoon"></span></a>
                    <a href="javascript:void(0)"><span class="icon-linkedin icomoon"></span></a>
                    <a href="javascript:void(0)"><span class="icon-linkedin icomoon"></span></a>       
                </div>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row justify-content-center rightsrow">
            <div class="col-md-6 text-center">
                <p>© 2020 GRAVTON MOTORS</p>
            </div>
        </div>
        <!--/ row -->
    </div>
    <!--/ container fluid -->
    <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
</footer>
<!--/ footer -->

<script>

</script>