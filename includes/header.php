<!-- header -->
<header class="bsnav-sticky bsnav-sticky-slide">
    <!-- nav -->
    <div class="navbar navbar-expand-lg bsnav cust-container">
            <a class="navbar-brand" href="index.php">               
                <img src="img/logo.svg" alt="" class="img-fluid">
            </a>
        <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse justify-content-md-end">
          <ul class="navbar-nav navbar-mobile mr-0">                      
            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'active';}else {echo'nav-link';} ?>" href="index.php">Home</a></li>
            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='quantax.php'){echo'active';}else {echo'nav-link';} ?>" href="quantax.php">Quanta-X</a></li> 
            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='quantas.php'){echo'active';}else {echo'nav-link';} ?>" href="quantas.php">Quanta-S</a></li> 
            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='swapeco-system.php'){echo'active';}else {echo'nav-link';} ?>" href="swapeco-system.php">Swap Eco-System</a></li> 
            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='whyquanta.php'){echo'active';}else {echo'nav-link';} ?>" href="whyquanta.php">Why Quanta</a></li> 
            <li class="nav-item"><a class="nav-link link-border" href="booknow.php">Book Now</a></li>  
            <li class="nav-item"><a class="nav-link " href="login.php"><span class="icon-user"></span></a></li>
            <li class="nav-item"><a class="nav-link " href="#" data-toggle="modal" data-target="#popNav"><span class="icon-bars"></span></a></li> 
          </ul>
        </div>
    </div>     
    <div class="bsnav-mobile">
      <div class="bsnav-mobile-overlay"></div>
      <div class="navbar"></div>
    </div>
    <!--/ nav -->  
</header>
<!--/ header -->

<!-- Modal -->
<div id="popNav" class="modal fade right" role="dialog">
    <div class="modal-dialog">  
      <!-- Modal content-->
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!-- modal body -->
        <div class="modal-body">
            <div class="nav-right">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>                   
                    <li><a href="bloglist.php">Media</a></li>
                    <li><a href="faq.php">Faq</a></li>                    
                    <li><a href="dealer.php">Become Dealer</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
                <p class="right-social">
                    <a href="javascript:void(0)"><span class="icon-facebook icomoon"></span></a>
                    <a href="javascript:void(0)"><span class="icon-twitter icomoon"></span></a>
                    <a href="javascript:void(0)"><span class="icon-linkedin icomoon"></span></a>
                    <a href="javascript:void(0)"><span class="icon-linkedin icomoon"></span></a>
                </p>
            </div>           
        </div>
        <!-- modal body -->       
      </div>
      <!-- mdal content -->  
    </div>
  </div>
  <!--/ modal -->