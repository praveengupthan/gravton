 <!-- nav -->
 <nav class="sb-topnav navbar navbar-expand navbar-dark">
        <a class="navbar-brand" href="index.php">
            <img src="img/logo.png" alt="">
        </a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><span class="icon-align-left icomoon"></span></button>
       
        <!-- Navbar-->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Username <i class="fas fa-user fa-fw"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">                   
                    <a class="dropdown-item" href="#">My Profile</a>
                    <a class="dropdown-item" href="#">Change Password</a>
                    <a class="dropdown-item" href="#">Pre Orders</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php">Logout</a>
                </div>
            </li>
        </ul>
    </nav>
    <!--/ nav -->