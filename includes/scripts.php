<!-- scripts -->
<script src="node_modules/jquery/dist/jquery.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="node_modules/popper.js/dist/esm/popper.min.js"></script>
<script src="node_modules/swiper/swiper-bundle.min.js"></script>
<script src="node_modules/easy-responsive-tabs/js/easyResponsiveTabs.js"></script>
<script src="node_modules/wowjs-person/dist/wow.min.js"></script>
<script src="js/bsnav.min.js"></script>
<script src="js/jquery.smoothScroll.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
 <!--[if lte IE 9]>
        <script src="node_modules/fix-ie/dist/ie.lteIE9.js"></script>
    <![endif]-->

<script>     
 //animation
wow = new WOW(
    {
        animateClass: 'animate__animated',
        offset: 100,
        mobile: true
    }
);
wow.init(); 
</script>
<script src="js/stepper.js"></script>

<script src="js/custom.js"></script>
<!--/ scripts -->

