<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
 <!--  styles -->
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="node_modules/swiper/swiper-bundle.min.css">
<link rel="stylesheet" href="node_modules/animate.css/animate.min.css">
<link rel="stylesheet" href="node_modules/easy-responsive-tabs/css/easy-responsive-tabs.css">
<link rel="stylesheet" href="css/bsnav.min.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/fonts.css">   
<link rel="stylesheet" href="css/timeline.css">
<link rel="stylesheet" href="css/stepper.css">
<link rel="stylesheet" href="css/userdb.css">
<link rel="stylesheet" href="css/style.css">
<!--/ styles -->