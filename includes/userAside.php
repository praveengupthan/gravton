<!-- left nav -->
<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">                       
                <a class="nav-link" href="userProfile.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    My profile
                </a>  
                <a class="nav-link" href="userPreOrders.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Pre Orders
                </a>   
                <a class="nav-link" href="userManageAddress.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Manage Address
                </a>   
                <a class="nav-link" href="userChangePassword.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                   Change Password
                </a>    
                <!-- <a class="nav-link" href="#">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                   My Orders
                </a>  -->

                <!--      
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    My Orders
                    <div class="sb-sidenav-collapse-arrow"><span class="icon-caret-down"></span></div>
                </a>
               <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="javascript:void(0)">Bikes</a>
                        <a class="nav-link" href="javascript:void(0)">Spare Parts</a>
                    </nav>
                </div> -->
                
                <!-- <a class="nav-link" href="savedCards.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                     Saved Cards
                </a> -->
                <a class="nav-link" href="index.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                   Logout
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Copyrights@</div>Gravton
        </div>
    </nav>
</div>
<!--/ left nav -->