<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>
</head>

<body class="homd-body">

<?php 
    include 'includes/header.php';
?>

<div class="links d-none d-lg-block">
    <a id="a1" data-id="a1" href="#1"></a>
    <a id="a2" data-id="a2" href="#2"></a>
    <a id="a3" data-id="a3" href="#3"></a>
    <a id="a4" data-id="a4" href="#4"></a>       
    <a id="a5" data-id="a5" href="#5"></a>      
    <a id="a6" data-id="a6" href="#6"></a> 
</div>

<!-- main -->
<!-- video render -->
<section class="video-home" id="1">
    <video autoplay muted loop>
        <source src="http://gravton.corewebpro.com/videos/homepageTransition-2.mp4" type="video/mp4">
    </video>    
</section>
<!--/ video render -->

<!-- swapping video -->
<section class="swapping-section" id="2">  
   
        <video autoplay muted loop>
            <source src="http://gravton.corewebpro.com/videos/batteryVideo.mp4" type="video/mp4">
        </video>
        <!-- swapright -->
        <div class="swapright">
            <article class="article swap-article">
                <h4 class="text-uppercase animate__animated animate__fadeInUp wow fwhite">Battery Swapping on the Go!</h4>
                <h2 class="animate__animated animate__fadeInUp wow pb-2">
                    Where
                    <br>
                    The Going
                    <br>
                    Never Gets
                    <br>
                    Tough
                </h2>
                <a class="greenbrdBtn animate__animated animate__fadeInUp wow" href="swapeco-system.php">Know More</a>

            </article>
        </div>
        <!--/ swapright -->  
  
</section>
<!--/ swapping video -->



<!-- gear up -->
<section class="gearup container" id="3">
 <div class="gearin">
        <!-- row -->
        <div class="d-sm-flex justify-content-end">
            <!-- col -->
            <div class="align-self-right gearinarticle">
                <div class="article">                    
                    <h2 class="h2 text-uppercase animate__animated animate__fadeInUp wow">
                       Eco System
                    </h2>
                    <p class="animate__animated animate__fadeInUp wow pb-4">Gravton Quanta is robust to the core to meet your drive for adventure. It is engineered to be sturdy with a suspension system that provides control
                    and comfort. Brace Yourself for a performance that's worth an applause.</p>                   
                </div>
            </div>
            <!--/ col -->           
        </div>
        <!--/ row -->
    </div>
</section>
<!--/ gear uup -->

<!-- smart system mobile app -->
<section class="smart-mobile" id="4">
    <!-- container -->
    <div class="cust-container">
         <!-- row -->
        <div class="row justify-content-end">
            <div class="col-md-4 text-center align-self-center order-md-last">
                <div class="article">              
                    <h2 class="h2 text-uppercase animate__animated animate__fadeInUp wow fgreen">Performance</h2>
                    <p class="fwhite animate__animated animate__fadeInUp wow pb-3">India's first rib caged chassis designed with low center of gravity for handling paired with apowerful 3KW Hub motor making the vehicle nimble</p>
                </div>    
                       
            </div>            
        </div>
        <!--/ row -->

       
    </div>
    <!--/ container -->
</section>
<!--/ smarrt system mobilke app -->

<!-- kms section -->
<section class="kms-section" id="5">
    <!-- cust container -->
    <div class="cust-container">
        <!-- row -->
        <div class="row justify-content-center">
            <!-- col -->
            <div class="col-md-10 text-center">
                <div class="article">
                    <h4 class="text-uppercase animate__animated animate__fadeInUp wow">Durable , RELIABLE &amp;, ECONOMICAL</h4>
                    <h1 class="h2 text-uppercase animate__animated animate__fadeInUp wow">800KMS</h1>
                    <h3 class="h3 animate__animated animate__fadeInUp wow">For Just Rs.80</h3>
                </div>
                <img src="img/bike2new.png" alt="" class="img-fluid kmsimg">

            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ cust container -->
</section>
<!--/ kms section -->

<!-- proudly india -->
<div class="proudlyIndia">
    <div class="container">
        <div class="row">
            <div class="col-md-6 align-self-center">
                <div class="article">
                    <h1 class="text-uppercase animate__animated animate__fadeInUp wow">Proudly Made in India</h1>
                    <p class="h6 animate__animated animate__fadeInUp wow">All component of Quanta have been designed, engineered and manufactured in india to make people belive in electric vehicle is the real alternative by addressing the range anxiety with our solutions</p>
                </div>
            </div>
            <div class="col-md-6">
                <img src="img/indiamap.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</div>
<!--/ proudly india -->


<!-- journey -->
<section class="journey" id="6">
    <!-- container -->
    <div class="container text-center">      
       
        <!-- row -->
        <div class="row justify-content-center">
            <div class="col-md-8 text-center">
                <div class="article">
                   
                    <h2 class="h2 text-uppercase animate__animated animate__fadeInUp wow">Order Your <br> 
                        Quant Now</h2>                   
                    <a class="greenBtn animate__animated animate__fadeInUp wow fblack" href="javascript:void(0)">Order Now</a>                   
                </div>               
            </div>
        </div>
        <!--/ row -->
        
    </div>
    <!--/ container -->
</section>
<!--/ journey -->


<!--/ main -->
<?php 
    include 'includes/footer.php';
?>


<?php 
    include 'includes/scripts.php';
?>

<script>

$( 'a[href^="#"]' ).SmoothScroll( {
        duration: 1000,
        easing  : 'easeOutQuint'
    } );
    

       //jquery smooth scroll
    $('.links a').click(function(){	
      var anchorId = "#"+ $(this).attr("data-id");
      $('.links a').removeClass('Select-nav');
      $(anchorId).toggleClass('Select-nav');     
  });

</script>


    
</body>
</html>
