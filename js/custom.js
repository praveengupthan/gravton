




//header add class
$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $("header").addClass("fixed-top");
    } else {
        $("header").removeClass("fixed-top");
    }
});




//on click move to browser top
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#movetop').fadeIn()
        } else {
            $('#movetop').fadeOut();
        }
    });

    //click event to scroll to top
    $('#movetop').click(function () {
        $('html, body').animate({ scrollTop: 0 }, 400);
    });

    //click vehicle add class
});



//quanta vertical slider
var swiper = new Swiper('.quanta-slider', {
    pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
    },
});

$(document).ready(function () {
    //Vertical Tab
    $('#spectab').easyResponsiveTabs({
        type: 'vertical', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        closed: 'accordion', // Start closed if in accordion view
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo2');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
});

//colours swiper 
var swiper = new Swiper('.colours-swiper', {
    spaceBetween: 30,
    effect: 'fade',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});


//user dashboard
(function ($) {

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $("#layoutSidenav_nav .sb-sidenav a.nav-link").each(function () {
        if (this.href === path) {
            $(this).addClass("active");
        }
    });

    // Toggle the side navigation
    $("#sidebarToggle").on("click", function (e) {
        e.preventDefault();
        $("body").toggleClass("sb-sidenav-toggled");
    });
})(jQuery);






