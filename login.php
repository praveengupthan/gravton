<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>
    
</head>



<!-- login main -->
<section class="loginmain">
    <div class="login-col">
        <a href="index.php" class="loginbrand">
            <img src="img/logo.png" alt="">
        </a>
        <form class="form-login" method="">
            <div class="form-group">
                <label>Username</label>
               <div class="input-group">
                    <input type="text" class="form-control" placeholder="Username / Mobile Number">
               </div>
            </div>
            <div class="form-group">
                <label>Password</label>
               <div class="input-group">
                    <input type="password" class="form-control" placeholder="Password">
               </div>
            </div>
            <div class="form-group">              
                <input onclick="window.location.href='userProfile.php';" type="button" class="btn" value="Sign in">
            </div>
        </form>
        <p><a href="forgotpw.php">Forgot Password?</a></p>
        <p>Don't have an account? <a href="signup.php">Signup</a></p>
    </div>
</section>
<!--/ login main -->



<!--/ main -->


<?php 
    include 'includes/scripts.php';
?>


<body>
    
</body>
</html>
