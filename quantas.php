<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>
    
</head>

<body>

<?php 
    include 'includes/header.php';
?>

<!-- main -->
<main class="subMain">

<!-- slider -->
<section class="quantaslider"> 
    <img src="img/quantasban01.png" alt="" class="img-fluid">
</section>
<!--/ slider -->

<!-- rotate quanta -->
<section class="rotateQuanta">

    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row justify-content-center pb-4">
            <!-- col -->
            <div class="col-md-4 col-sm-4 text-center rotatecol">
                <h4 class="animate__animated animate__fadeInUp wow">Fast</h4>
                <h2 class="animate__animated animate__fadeInUp wow ">70<span> KMS</span></h2>
                <p class="animate__animated animate__fadeInUp wow">Top Speed</p>
            </div>
            <!--/ col -->
             <!-- col -->
             <div class="col-md-4 col-sm-4 text-center rotatecol">
                <h4 class="animate__animated animate__fadeInUp wow">Quick</h4>
                <h2 class="animate__animated animate__fadeInUp wow ">45<span> MIN</span></h2>
                <p class="animate__animated animate__fadeInUp wow">Quick Battery Charging</p>
            </div>
            <!--/ col -->
             <!-- col -->
            <div class="col-md-4 col-sm-4 text-center rotatecol">
                <h4 class="animate__animated animate__fadeInUp wow">RELIABLE</h4>
                <h2 class="animate__animated animate__fadeInUp wow ">120<span> KMS</span></h2>
                <p class="animate__animated animate__fadeInUp wow">With One-Time Charging</p>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->

        <!-- row -->
        <div class="row justify-content-center">
            <!-- col -->
            <div class="col-md-6 quantaRotate">
                <video controls>
                    <source src="http://gravton.corewebpro.com/videos/QuantaSRotate.mp4" type="video/mp4">
                    <source src="http://gravton.corewebpro.com/videos/QuantaSRotate.ogg" type="video/ogg">
                </video>
            </div>
            <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->   

</section>
<!--/ rotate quanta -->

<!-- colours -->
<section class="coloursBikes">
        <!-- container -->
        <div class="container">
            <h1 class="h1 text-uppercase animate__animated animate__fadeInUp wow">Quanta</h1>
            <!-- Swiper -->
            <div id="bikescolours" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
                <ol class="carousel-indicators">
                    <li data-target="#bikescolours" data-slide-to="0" class="active"></li>
                    <li data-target="#bikescolours" data-slide-to="1"></li>
                    <li data-target="#bikescolours" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img class="d-block w-100" src="img/qx-color-black.png" alt="black" class="img-fluid">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="img/qx-color-red.png" alt="black" class="img-fluid">
                    </div>
                    <div class="carousel-item">
                    <img class="d-block w-100" src="img/qx-color-gray.png" alt="black" class="img-fluid">
                    </div>
                </div>
               
                </div>
            <!-- swiper -->
        </div>
        <!--/container -->
</section>
<!--/ colours -->

 <!-- standards -->
 <section class="standardsQuanta">
          <!-- container -->
          <div class="container">
                <!-- row -->
                <div class="row justify-content-center pb-5">
                    <div class="col-md-8 text-center">
                        <div class="article">                        
                            <h2 class="h2 text-uppercase animate__animated animate__fadeInUp wow fgreen">A NEW STANDARD IN ELECTRIC.</h2>
                            <p class="fwhite animate__animated animate__fadeInUp wow">The e-volution that you deserve is here! Gravton Quanta is engineered to set a benchmark for electric in performance, range, and battery. All in a head-turning style.</p>
                        </div>
                    </div>
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6 col-sm-6 px-0">
                        <img src="img/qs-standard01.png" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-6 col-sm-6 starticle align-self-center animate__animated animate__fadeInUp wow">
                        <h3>Designed to delight</h3>
                        <ul class="list-items">
                            <li>Ideal for fun and adventure</li>
                            <li>Stylish and sleek vehicle layout</li>
                            <li>All-terrain tuned suspension</li>
                            <li>Low centre of gravity and highstability</li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row">
                    <!-- col -->
                    <div class="col-md-6 col-sm-6 order-md-last px-0">
                        <img src="img/qs-standard02.png" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-6 col-sm-6 starticle align-self-center animate__animated animate__fadeInUp wow">
                        <h3>Path - paving performance</h3>
                        <ul class="list-items">
                            <li>Pulling ability upto 300 Kgs</li>
                            <li>3 Speed modes (Eco, Drive, Jet)</li>
                            <li> Most efficient electric drive-train system with minimal mechanical losses</li>
                           
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6 col-sm-6 px-0">
                        <img src="img/qs-standard03.png" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-6 col-sm-6 starticle align-self-center animate__animated animate__fadeInUp wow">
                        <h3>Smart swap ecosystem</h3>
                        <ul class="list-items">
                            <li>Battery Swap with a tap Just place the request on our app</li>
                            <li>Live location and vehicle tracking</li>
                            <li>Battery swap on the go Super responsive service network</li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row">
                    <!-- col -->
                    <div class="col-md-6 col-sm-6 order-md-last px-0">
                        <img src="img/qs-standard04.png" alt="" class="img-fluid">
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-6 col-sm-6 starticle align-self-center animate__animated animate__fadeInUp wow">
                        <h3>Superior Battery</h3>
                        <ul class="list-items">
                            <li>Fast charge in 45 mins</li>
                            <li>Full charge in 2.5 hours</li>
                            <li>Extra battery slot for longer trips</li>                            
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->                
          </div>
          <!--/ container -->  
    </section>
    <!--/ standars -->

<!-- specs -->
<section class="specs-detail">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row justify-content-between ">
            <!-- col -->
            <div class="col-md-8">
                <h1 class="h1 text-uppercase fwhite text-center text-md-left">Specifications</h1>
            </div>
            <!-- col -->
            <!-- col -->
            <div class="col-md-4 text-md-right text-center">
                <a class="greenbrdBtn" href="img/dummy.pdf" download>Download Brochure</a>
            </div>
            <!-- col -->
        </div>
        <!--/ row -->


        <!-- spec tab -->       
        <div class="spectab qs">
            <!--Vertical Tab-->
            <div id="spectab">
                <ul class="resp-tabs-list hor_1">
                    <li>Performance</li>
                    <li>Range</li>
                    <li>Charging</li>
                    <li>Battery</li>                   
                    <li>Dimension</li>
                    <li>Smart Features</li>
                    <li>APB Based Smart Features</li>
                </ul>
                <div class="resp-tabs-container hor_1">

                    <!-- Performance-->
                    <div>
                        <h5>Performance</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Power(Continuous / Peak)</dt>
                                <dd>2 KW/ 4 KW</dd>
                            </div>
                            <!--/ col -->                            
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Max Torque</dt>
                                <dd>180 Nm at Wheel</dd>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Top Speed</dt>
                                <dd>70 Kmph</dd>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Acceleration (0-40 kmph)</dt>
                                <dd>4.2 s</dd>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Gradeability</dt>
                                <dd>26 Degrees</dd>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Riding Modes</dt>
                                <dd>BLDC</dd>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6">
                                <dt>Acceleration (0-40 kmph)</dt>
                                <dd>Eco (120 km), City (Drive) (90-96 km), Jet (82 Km)</dd>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-6">
                                <dt>Motor</dt>
                                <dd>BLDC</dd>
                            </div>
                            <!--/ col -->
                        </div>
                    </div>
                     <!--/ Performance-->

                    <!-- Range-->
                    <div>
                    <h5>Range</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Range</dt>
                                <dd>120 km</dd>
                            </div>
                            <!--/ col -->                            
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>With Extendable Range</dt>
                                <dd>326 km</dd>                               
                            </div>
                            <!--/ col -->                           
                        </div>
                    </div>
                     <!-- /Range-->

                    <!-- Charging-->
                    <div> 
                    <h5>Charging</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>0-50% (Fast Charging)</dt>
                                <dd>45 Min</dd>
                            </div>
                            <!--/ col -->                            
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>0-80% (Home Charging)</dt>
                                <dd>2 Hours</dd>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>0-100% (Home Charging)</dt>
                                <dd>2.5 Hours</dd>
                            </div>
                            <!--/ col -->                           
                        </div>
                    </div>
                    <!-- /Charging-->

                    <!-- Battery-->
                    <div> 
                    <h5>Battery</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Installed Capacity ( 1 Battery Pack)</dt>
                                <dd>1.8 kWh (Kilo WattHour)</dd>
                            </div>
                            <!--/ col -->                            
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Extendable (1 + 2)</dt>
                                <dd>Upto 5.5 kWh</dd>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Battery Type</dt>
                                <dd>Li-ion</dd>
                            </div>
                            <!--/ col -->                          
                        </div>
                    </div>
                    <!-- /Battery-->                   
                    
                    <!-- Dimension-->
                    <div>
                    <h5>Dimension</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>Kerb Weight</dt>
                                <dd>94 kg</dd>
                            </div>
                            <!--/ col -->                            
                            <!-- col -->
                            <div class="col-md-6">
                                <dt>F:R Weight Ratio</dt>
                                <dd>47 : 53</dd>
                            </div>
                            <!--/ col -->
                          
                        </div>
                    </div>
                    <!-- /Dimension-->

                      <!-- Smart Features-->
                      <div> 
                      <h5>Smart Features</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-12">
                                <dt>Connectivity</dt>
                                <dd>GOTAC (Gravton Over the Air Controller) -
                                communicates the overall health and whereabouts of the bike to the mobile app via a server in the
                                cloud.</dd>
                            </div>
                            <!--/ col --> 
                        </div>
                    </div>
                    <!-- /Smart Features-->

                    <!-- APB Based Smart Features-->
                    <div> 
                    <h5>APB Based Smart Features</h5>
                        <div class="row pt-3 specrow">
                            <!-- col -->
                            <div class="col-md-12">
                                <dt>Mobile App</dt>
                                <dd>
                                    <ul>
                                        <li>Ride Stats</li>
                                        <li>Push Navigation</li>
                                        <li>Live Location and Vehicle State Tracking</li>
                                        <li>Theft/Tow Detection</li>
                                        <li>Battery Status - Kms left to ride</li>
                                        <li>State of Charge (SOC)</li>
                                        <li>Know your battery - battery cycles</li>
                                        <li>Battery Governor</li>
                                    </ul>
                                </dd>
                            </div>
                            <!--/ col --> 
                        </div>
                    </div>
                    <!-- /APB Based Smart Features-->
                    
                </div>
            </div>
            <!--/ vertical tab -->
        </div>
        <!--/ spec tab -->
    </div>
    <!--/ container -->
    </section>
    <!--/ specs -->

   

    <!-- mobile connectivity -->
    <section class="mob-connectivity">
        <!--container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-4 align-self-center">
                    <div class="article">                        
                        <h2 class="h4 text-uppercase animate__animated animate__fadeInUp wow fred">Connectivity</h2>
                        <p class="fwhite animate__animated animate__fadeInUp wow">GOTAC (Gravton Over the Air Controller) - communicates the overall health and whereabouts of the bike to the mobile app via a server in the cloud.</p>
                    </div>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-4">
                     <img src="img/mob-qx.png" alt="" class="img-fluid">
                 </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-4 align-self-center">
                    <div class="article">                        
                        <h2 class="h4 text-uppercase animate__animated animate__fadeInUp wow fred">APP BASED SMART FEATURES</h2>
                        <ul class="list-items animate__animated animate__fadeInUp wow">
                            <li>Ride Stats</li>
                            <li>Push Navigation</li>
                            <li>Live Location and Vehicle State Tracking</li>
                            <li>Theft/Tow Detection</li>
                            <li>Battery Status - Kms left to ride</li>
                            <li>State of Charge (SOC)</li>
                            <li>Know your battery - battery cycles</li>
                            <li>Battery Governor</li>
                        </ul>
                    </div>
                 </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </section>
    <!--/ mobilke connectivity -->



            


    <!-- image gallery -->
    <div class="quantaGallery">
        <!-- container fluid -->
        <div class="container-fluid no-gutters px-0">
            <!-- row -->
            <div class="row no-gutters">
                <!-- col -->
                <div class="col-md-2 col-6 galcol zoom-box">
                    <img src="img/qs-gallery/qs-gallery01.jpg" alt="" class="img-fluid galimg">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-md-4 col-6">
                    <!-- row -->
                    <div class="row no-gutters h50">
                        <div class="col-md-6 col-12 galcol zoom-box">
                            <img src="img/qs-gallery/qs-gallery02.jpg" alt="" class="img-fluid galimg">
                        </div>
                        <div class="col-md-6 col-12 galcol zoom-box">
                            <img src="img/qs-gallery/qs-gallery03.jpg" alt="" class="img-fluid galimg">
                        </div>                       
                    </div>
                    <!--/ row -->
                     <!-- row -->
                     <div class="row no-gutters h50">
                        <div class="col-md-12 galcol zoom-box">
                            <img src="img/qs-gallery/qs-gallery04.jpg" alt="" class="img-fluid galimg">
                        </div>                                           
                    </div>
                    <!--/ row -->
                </div>
                 <!--/ col -->
                <!-- col -->
                <div class="col-md-2 col-6 galcol zoom-box">
                    <img src="img/qs-gallery/qs-gallery05.jpg" alt="" class="img-fluid galimg">
                </div>
                 <!--/ col -->
                  <!-- col -->
                <div class="col-md-4 col-6">
                    <!-- row -->
                    <div class="row no-gutters">                       
                        <div class="col-md-12 col-12 galcol zoom-box">
                            <img src="img/qs-gallery/qs-gallery06.jpg" alt="" class="img-fluid galimg">
                        </div>
                        <div class="col-md-12 col-12 galcol zoom-box">
                            <img src="img/qs-gallery/qs-gallery07.jpg" alt="" class="img-fluid galimg">
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                 <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container fluid -->
    </div>
    <!--/ image gallery -->


</main>




<!--/ main -->
<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
