<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    
</head>

<body class="sb-nav-fixed user-screen">

<!-- main -->
<div id="layoutSidenav">
        <?php
            include 'includes/headerPostlogin.php';
        ?>

        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-flex justify-content-between pb-2 border-bottom userRightTitle">
                    <h3 class="mt-2 fbold h4 mb-0 pb-0">Manage Saved Cards</h3>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a href="userProfile.php">Praveen Kumar</a></li>
                        <li class="breadcrumb-item active">Manage Saved Cards</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">  
                         <!-- row -->
                         <div class="row justify-content-center pt-5">
                            <!-- col -->
                            <div class="col-md-6">
                                <p class="pb-3">
                                    <a data-toggle="modal" data-target="#NewSavedCard"  href="javascript:void(0)" class="blackbrdBtn w-100 text-center"><span class="icon-plus icomoon"></span> Add New Card</a>
                                </p>

                                <ul class="list-group">
                                    <li class="list-group-item address-item">                                       
                                        <p class="py-2">
                                            <b class="">
                                                <span>Praveen Kumar</span>                                                
                                            </b>
                                        </p>
                                        <p class="d-flex cardnumber align-self-center">
                                            <img src="img/visa.svg" alt="">
                                            <span class="d-inline-block pt-1">4018 06** **** 7997</span>
                                        </p>
                                        <p class="pt-3 editicons text-right">
                                            <a href="javascript:void(0)">Edit</a>  |
                                            <a href="javascript:void(0)">Delete</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container fluid -->                    

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->
<?php 
    include 'includes/scripts.php';
?>



<!-- New Address -->
<div class="modal fade" id="NewSavedCard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add New Card</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- form -->
        <form class="form pt-2" method="">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>Enter Card Number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="" >
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>Valid Thru</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option>Month</option>
                                            <option>January</option>
                                            <option>February</option>
                                            <option>March</option>
                                            <option>April</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <select class="form-control">
                                            <option>Year</option>
                                            <option>21</option>
                                            <option>22</option>
                                            <option>23</option>
                                            <option>24</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>Name on Card</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="" >
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                    

                <!-- col -->
                <div class="col-md-12 pt-2">                           
                    <button class="btn greenBtn w-100">Save this Card</button>    
                    <p><small>Your card details would be securely saved for faster payments. Your CVV will not be stored</small></p>                        
                </div>
                <!--/ col -->
                 <!-- col 12 -->
                 <div class="col-md-12">                            
                <!-- faq -->
                <div class="faqs pt-5">
                    <h3 class="fgreen">Faq</h3>
                    <div class="faqItem">
                        <h6>Why is my Card being saved on Gravton?</h6>
                        <p>YIt's quicker. You can save the hassle of typing in the complete card information every time you shop at Flipkart by saving your card details. You can make your payment by selecting the saved card of your choice at checkout. While this is obviously faster, it is also very secure.</p>
                    </div>
                    <div class="faqItem">
                        <h6>Is it safe to save my cards on Gravton?</h6>
                        <p>Absolutely. Your card information is 100 percent safe with us. We use world class encryption technology while saving your card information on our highly secure systems. Our payment systems have passed stringent security compliance checks like PCI DSS (Payment Card Industry Data Security Standards) to ensure that your card information remains confidential at all times.</p>
                    </div>
                    <div class="faqItem">
                        <h6>What all card information does Gravton?</h6>
                        <p>Gravton only stores card information when the customer selects the option. We only store your card number, cardholder name and card expiry date. We do not store your card’s CVV number or the 3D Secure password.</p>
                    </div>
                    <div class="faqItem">
                        <h6>Can I delete my saved cards?</h6>
                        <p>Yes, you can delete your saved cards at any given time.</p>
                    </div>
                </div>
                <!--/ faq -->
                </div>
                <!--/ col 123 -->
                </div>
                <!--/ row -->
            </form>
            <!--/ form -->
      </div>
      
    </div>
  </div>
</div>

    
</body>
</html>
