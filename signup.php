<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>
    
</head>



<!-- login main -->
<section class="loginmain">
    <div class="login-col">
        <a href="index.php" class="loginbrand">
            <img src="img/logo.png" alt="">
        </a>
        <form class="form-login" method="">
            <div class="form-group">
                <label>First Name</label>
               <div class="input-group">
                    <input type="text" class="form-control" placeholder="First Name">
               </div>
            </div>
            <div class="form-group">
                <label>Last Name</label>
               <div class="input-group">
                    <input type="text" class="form-control" placeholder="Last Name">
               </div>
            </div>
            <div class="form-group">
                <label>Mobile Number</label>
               <div class="input-group">
                    <input type="text" class="form-control" placeholder="Mobile Number">
               </div>
            </div>
            <div class="form-group">
                <label>Email</label>
               <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email">
               </div>
            </div>
            <div class="form-group">
                <label>Password</label>
               <div class="input-group">
                    <input type="password" class="form-control" placeholder="Enter Password">
               </div>
            </div>
            <div class="form-group">              
                <input type="submit" class="btn" value="Create An Account">
            </div>
        </form>       
        <p>Have an account? <a href="login.php">Sign in</a></p>
    </div>
</section>
<!--/ login main -->



<!--/ main -->


<?php 
    include 'includes/scripts.php';
?>


<body>
    
</body>
</html>
