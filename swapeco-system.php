<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain otherpage">

<!-- container -->
<div class="container">

<!-- title row -->
<div class="row pt-sm-5 pt-2">
    <!-- col -->
    <div class="col-md-12">
        <div class="article pt-2 pt-lg-5 pb-3 d-flex justify-content-between">                        
            <h2 class="h4 pb-md-4 text-uppercase fgreen">Swap Eco System</h2> 
        </div>
    </div>
    <!-- col -->        
</div>
<!-- title row -->

<!-- row -->
<div class="row py-4">
    <!-- col -->
    <div class="col-md-6 ecocol">
        <img src="img/app01.jpeg" alt="" class="img-fluid swapimg">
        <div class="article p-3 align-self-center">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Eco Title</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem.</p>
        </div>
    </div>
    <!--/ col -->

   <!-- col -->
   <div class="col-md-6 ecocol">
        <img src="img/app01.jpeg" alt="" class="img-fluid swapimg">
        <div class="article p-3 align-self-center">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Eco Title</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem.</p>
        </div>
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6 ecocol">
        <img src="img/app01.jpeg" alt="" class="img-fluid swapimg">
        <div class="article p-3 align-self-center">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Eco Title</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem.</p>
        </div>
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6 ecocol">
        <img src="img/app01.jpeg" alt="" class="img-fluid swapimg">
        <div class="article p-3 align-self-center">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Eco Title</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem.</p>
        </div>
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6 ecocol">
        <img src="img/app01.jpeg" alt="" class="img-fluid swapimg">
        <div class="article p-3 align-self-center">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Eco Title</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem.</p>
        </div>
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6 ecocol">
        <img src="img/app01.jpeg" alt="" class="img-fluid swapimg">
        <div class="article p-3 align-self-center">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Eco Title</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem.</p>
        </div>
    </div>
    <!--/ col -->
</div>
<!--/ row -->









</div>
<!--/ container -->
   
    
  
    

    
       
      
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
