<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain">
    <!-- container -->
    <div class="container">

    <!-- title row -->
    <div class="row ">
        <!-- col -->
        <div class="col-md-12">
            <div class="article pb-3 d-flex justify-content-between">                        
                <h2 class="h4 pb-4 text-uppercase fgreen">Terms</h2>                    
               
            </div>
        </div>
        <!-- col -->        
    </div>
    <!-- title row -->

    <!-- row -->
    <div class="row justify-content-center blogdetail-col">
        <div class="col-md-12">

        <p>We collect information you provide directly to us. For example, you share information directly with us when you create an account, fill out a form, submit or post content through our Services, purchase a membership, communicate with us via third-party platforms, request customer support, or otherwise communicate with us. The types of personal information we may collect include your name, display name, username, bio, email address, business information, your content, including your avatar image, photos, posts, responses, and series published by you, and any other information you choose to provide.</p>

        <p>In some cases, we may also collect information you provide about others, such as when you purchase a Medium membership as a gift for someone. We will use this information to fulfill your request and will not send communications to your contacts unrelated to your request, unless they separately consent to receive communications from us or otherwise engage with us.</p>

        <p>We do not collect payment information through our Services. We rely on third parties to process payments in connection with our Services. Any information you provide to facilitate such a payment is subject to the third-party payment processor’s privacy policy, and we encourage you to review this policy before you provide any information to the payment processor.</p>

        <p>We obtain information from third-party sources. For example, we may collect information about you from social networks, accounting services providers and data analytics providers. Additionally, if you create or log into your Medium account through a third-party platform (such as Apple, Facebook, Google, or Twitter), we will have access to certain information from that platform, such as your name, lists of friends or followers, birthday, and profile picture, in accordance with the authorization procedures determined by such platform.</p>

        <h5 class="h4 fgreen">Information We Derive</h5>
        <p>We may derive information or draw inferences about you based on the information we collect. For example, we may make inferences about your location based on your IP address or infer reading preferences based on your reading history.</p>

        <p>We do not collect payment information through our Services. We rely on third parties to process payments in connection with our Services. Any information you provide to facilitate such a payment is subject to the third-party payment processor’s privacy policy, and we encourage you to review this policy before you provide any information to the payment processor.</p>

        <p>We obtain information from third-party sources. For example, we may collect information about you from social networks, accounting services providers and data analytics providers. Additionally, if you create or log into your Medium account through a third-party platform (such as Apple, Facebook, Google, or Twitter), we will have access to certain information from that platform, such as your name, lists of friends or followers, birthday, and profile picture, in accordance with the authorization procedures determined by such platform.</p>

          

        </div>
    </div>
    <!--/ row -->

    
       
        
    </div>
    <!--/ container -->
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
