<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    
</head>

<body class="sb-nav-fixed user-screen">

<!-- main -->
<div id="layoutSidenav">
        <?php
            include 'includes/headerPostlogin.php';
        ?>

        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-flex justify-content-between pb-2 border-bottom userRightTitle">
                    <h3 class="mt-2 fbold h4 mb-0 pb-0">Change Password</h3>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a href="userProfile.php">Praveen Kumar</a></li>
                        <li class="breadcrumb-item active">Change Password</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">                   
                        <!-- row -->
                        <div class="row justify-content-center pt-5">
                            <!-- col -->
                            <div class="col-md-6">
                                <!-- form -->
                                <form class="form pt-2" method="">
                                    <!-- row -->
                                    <div class="row">
                                         <!-- col -->
                                        <div class="col-md-12">
                                            <div class="form-group customForm">
                                                <label>Enter Existing Password</label>
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="" >
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-12">
                                            <div class="form-group customForm">
                                                <label>Enter New Password</label>
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="" >
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-12">
                                            <div class="form-group customForm">
                                                <label>Confirm new Password</label>
                                                <div class="input-group">
                                                    <input type="password" class="form-control" name="" >
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                        <div class="col-md-12 pt-2">                           
                                            <button class="btn greenBtn w-100">Save Password</utton>                            
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/ row -->
                                </form>                               
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container fluid -->                    

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->
<?php 
    include 'includes/scripts.php';
?>

    
</body>
</html>
