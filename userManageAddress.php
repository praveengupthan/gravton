<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    
</head>

<body class="sb-nav-fixed user-screen">

<!-- main -->
<div id="layoutSidenav">
        <?php
            include 'includes/headerPostlogin.php';
        ?>

        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-flex justify-content-between pb-2 border-bottom userRightTitle">
                    <h3 class="mt-2 fbold h4 mb-0 pb-0">Manage Address</h3>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a href="userProfile.php">Praveen Kumar</a></li>
                        <li class="breadcrumb-item active">Manage Address</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">                   
                        <!-- row -->
                        <div class="row justify-content-center pt-5">
                            <!-- col -->
                            <div class="col-md-8">
                                <p class="pb-3">
                                    <a data-toggle="modal" data-target="#newAddress"  href="javascript:void(0)" class="blackbrdBtn w-100 text-center"><span class="icon-plus icomoon"></span> Add New Address</a>
                                </p>

                                <ul class="list-group">
                                    <li class="list-group-item address-item">
                                        <span class="badge badge-secondary rounded-0">Home</span>
                                        <p class="py-2">
                                            <b class="d-flex justify-content-between">
                                                <span>Praveen Kumar</span> 
                                                <span>+ 7995165789</span>
                                            </b>
                                        </p>
                                        <p>Plot No:91, 4-32-41/51/1, Kamala Prasanna Nagar, Kukatpally, Hyderabad , Telangana - 500072, Allwyn Colony Phase 1, Road No:9, Hyderabad, Telangana - 500072</p>
                                        <p class="pt-3 editicons text-right">
                                            <a href="javascript:void(0)">Edit</a>  |
                                            <a href="javascript:void(0)">Delete</a>
                                        </p>

                                    </li>

                                    <li class="list-group-item address-item">
                                        <span class="badge badge-secondary rounded-0">Work</span>
                                        <p class="py-2">
                                            <b class="d-flex justify-content-between">
                                                <span>Praveen Guptha</span> 
                                                <span>+ 9642123254</span>
                                            </b>
                                        </p>
                                        <p>Plot No:91, 4-32-41/51/1, Kamala Prasanna Nagar, Kukatpally, Hyderabad , Telangana - 500072, Allwyn Colony Phase 1, Road No:9, Hyderabad, Telangana - 500072</p>
                                        <p class="pt-3 editicons text-right">
                                            <a href="javascript:void(0)">Edit</a>  |
                                            <a href="javascript:void(0)">Delete</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->
                    </div>
                    <!--/ container fluid -->                    

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->
<?php 
    include 'includes/scripts.php';
?>

<!-- New Address -->
<div class="modal fade" id="newAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- form -->
        <form class="form pt-2" method="">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>Name</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="" >
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>10 Digit Phone number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="" >
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>Pincode</label>
                            <div class="input-group">
                                <input type="number" class="form-control" name="" >
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6">
                        <div class="form-group customForm">
                            <label>Locality</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="" >
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-md-12">
                        <div class="form-group customForm">
                            <label>Address</label>
                            <div class="input-group">
                               <textarea class="form-control" style="height:100px" placeholder="Address/area/Street"></textarea>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-md-6">
                    <div class="form-group customForm">
                        <label>City/Dist/Town</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="" >
                        </div>
                    </div>
                </div>
                <!--/ col -->
                 <!-- col -->
                 <div class="col-md-6">
                    <div class="form-group customForm">
                        <label>Select State</label>
                        <div class="input-group">                           
                            <input list="cities" name="" id="citiesall" class="form-control">
                            <datalist id="cities">
                                <option value="Telangana">
                                <option value="Andhra Pradesh">
                                <option value="Kerala">
                                <option value="Tamilnadu">
                                <option value="Maharastra">
                            </datalist>                           
                        </div>
                    </div>
                </div>
                <!--/ col -->   
                
                 <!-- col -->
                 <div class="col-md-12">
                    <div class="form-group customForm">
                        <label class="d-block">Address Type</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                            <label class="form-check-label" for="inlineRadio1">Home</label>
                            </div>
                            <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label" for="inlineRadio2">Work</label>
                            </div>
                        </div>
                </div>
                <!--/ col -->         
               
                <!-- col -->
                <div class="col-md-12 pt-2">                           
                    <button class="btn greenBtn w-100">Submit</button>                            
                </div>
                <!--/ col -->
                </div>
                <!--/ row -->
            </form>
            <!--/ form -->
      </div>
      
    </div>
  </div>
</div>



    
</body>
</html>
