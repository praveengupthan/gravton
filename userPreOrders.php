<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    
</head>

<body class="sb-nav-fixed user-screen">

<!-- main -->
<div id="layoutSidenav">
        <?php
            include 'includes/headerPostlogin.php';
        ?>

        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-flex justify-content-between pb-2 border-bottom userRightTitle">
                    <h3 class="mt-2 fbold h4 mb-0 pb-0">Pre Orders</h3>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active"><a href="userProfile.php">Praveen Kumar</a></li>
                        <li class="breadcrumb-item active">Pre Order</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">    

                        <!-- row -->
                        <div class="row justify-content-center pt-4">
                            
                            <!-- col -->
                            <div class="col-md-6 text-center">
                                <div class="addIcon mx-auto my-2">
                                    <span class="icon-plus-circle icomoon"></span>
                                </div>
                                <h4><b>You Don't have any Pre Orders Right now</b></h4>
                                <p class="py-3">Once You Pre Booked Vehicle Confirmed and Token Amount payment done,  We will update the status of available of Your Preorder to your registered mobile number and Registered Email with details of Date of Delivery </p>
                                <a href="createPreOrder.php" class="greenBtn">Pre Order Now </a>
                            </div>
                            <!--/ col -->
                           
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row pt-5">
                            <!-- col -->
                            <div class="col-md-12">
                            <p class="text-right pb-3">
                                <a href="javascript:void(0)" class="greenBtn">Create Pre Order</a>
                            </p>
                                <div class="table-responsive">
                                    <table class="table table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name of Vehicle</th>
                                                <th scope="col">Date of Preorder</th>
                                                <th scope="col">On Whom Name</th>
                                                <th scope="col">Token Amount</th>
                                                <th scope="col">Payment Status</th>
                                                <th scope="col">Download Receipt</th>
                                                <th scope="col">Expected Delivery</th>
                                                <th scope="col">View </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>
                                                    <a href="quantas.php">Quanta X</a>
                                                </td>
                                                <td>25-11-2020</td>
                                                <td>Praveen Kumar</td>
                                                <td>Rs:5000.00</td>
                                                <td>Paid on 26-05-2020</td>
                                                <td>
                                                    <a class="d-block" href="javascript:void(0)" download><span class="icon-download h3 fgreen"></span></a>
                                                </td>
                                                <td>25-12-2020</td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#preOrderView" href="javascript:void(0)" class="greenBtn">View More</a>
                                                </td>
                                            </tr>
                                          
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ rw -->                     
                    </div>
                    <!--/ container fluid -->                    

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->
<?php 
    include 'includes/scripts.php';
?>

<!-- preorder view -->
<div class="modal fade" id="preOrderView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false"> 
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Quanta X</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <img src="img/viewmypreorderbike.png" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                  <!-- col -->
                <div class="col-md-6">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-md-6 preOrdercol">
                            <dt>Pre Order Vehicle</dt>
                            <dd>Quanta X</dd>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6 preOrdercol">
                            <dt>Date of Booked</dt>
                            <dd>25-11-2020</dd>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-6 preOrdercol">
                            <dt>Booked on Whom Name</dt>
                            <dd>Praveen</dd>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-md-6 preOrdercol">
                            <dt>Payment Status</dt>
                            <dd>Paid</dd>
                        </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-md-6 preOrdercol">
                            <dt>Token Amount</dt>
                            <dd>Rs:5,000</dd>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-md-6 preOrdercol">
                            <dt>Expected Delivery</dt>
                            <dd>25-12-2020</dd>
                        </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-md-12 preOrdercol pt-3">
                           <a href="javascript:void(0)" class="greenBtn">Cancel Pre Order</a>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ col -->
                <!-- col 12 -->
                <div class="col-md-12">                            
                <!-- faq -->
                <div class="faqs pt-5">
                    <h3 class="fgreen">Faq</h3>
                    <div class="faqItem">
                        <h6>What happens when I update my email address (or mobile number)?</h6>
                        <p>Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).</p>
                    </div>
                    <div class="faqItem">
                        <h6>When will my Gravton account be updated with the new email address (or mobile number)?</h6>
                        <p>It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.</p>
                    </div>
                    <div class="faqItem">
                        <h6>What happens to my existing Gravnton account when I update my email address (or mobile number)?</h6>
                        <p>Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.</p>
                    </div>
                </div>
                <!--/ faq -->
                </div>
                <!--/ col 123 -->
            </div>
            <!--/ row -->        
        </form>
      </div>
    
    </div>
  </div>
</div>
<!--/ Preorder view -->

    
</body>
</html>
