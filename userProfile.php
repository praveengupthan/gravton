<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    
</head>

<body class="sb-nav-fixed user-screen">

<!-- main -->
<div id="layoutSidenav">
        <?php
            include 'includes/headerPostlogin.php';
        ?>

        <?php 
            include 'includes/userAside.php';
        ?>

        <!-- right main -->
        <div id="layoutSidenav_content">
            <!-- main -->
            <main class="user-right">
                <!-- page title -->
                <div class="container-fluid d-flex justify-content-between pb-2 border-bottom userRightTitle">
                    <h3 class="mt-2 fbold h4 mb-0 pb-0">Personal Information</h3>
                    <ol class="breadcrumb mb-1 pb-0">
                        <li class="breadcrumb-item active"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">My profile</li>                        
                    </ol>   
                </div>
                <!--/ page title -->

                <!-- page body -->
                <section class="page-body">
                    <!-- container fluid -->
                    <div class="container-fluid">                   
                        <!-- row -->
                        <div class="row justify-content-center pt-5">
                            <!-- col -->
                            <div class="col-md-8">
                                <p class="text-right pb-3">
                                    <a href="javascript:void(0)" class="blackbrdBtn">Edit Profile</a>
                                </p>
                                <form class="form">
                                    <!-- row -->
                                    <div class="row">
                                         <!-- col -->
                                        <div class="col-md-6">
                                            <div class="form-group disformgroup">
                                                <label>First Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="First Name" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Last Name">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mobile Number</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Mobile Number">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Email">
                                                </div>
                                            </div>
                                        </div>
                                        <!--/ col -->
                                         <!-- col -->
                                         <div class="col-md-12">
                                            <input type="submit" class="btn greenBtn w-100" value="Save Profile">
                                        </div>
                                        <!--/ col -->
                                    </div>
                                    <!--/row -->
                                </form>

                                <!-- faq -->
                                <div class="faqs pt-5">
                                    <h3 class="fgreen">Faq</h3>
                                    <div class="faqItem">
                                        <h6>What happens when I update my email address (or mobile number)?</h6>
                                        <p>Your login email id (or mobile number) changes, likewise. You'll receive all your account related communication on your updated email address (or mobile number).</p>
                                    </div>
                                    <div class="faqItem">
                                        <h6>When will my Gravton account be updated with the new email address (or mobile number)?</h6>
                                        <p>It happens as soon as you confirm the verification code sent to your email (or mobile) and save the changes.</p>
                                    </div>
                                    <div class="faqItem">
                                        <h6>What happens to my existing Gravnton account when I update my email address (or mobile number)?</h6>
                                        <p>Updating your email address (or mobile number) doesn't invalidate your account. Your account remains fully functional. You'll continue seeing your Order history, saved information and personal details.</p>
                                    </div>
                                </div>
                                <!--/ faq -->
                            </div>
                            <!--/ col -->




                        </div>
                        <!--/ row -->



                    </div>
                    <!--/ container fluid -->                    

                </section>
                <!---/ page body -->
            </main>
            <!--/ main -->
        </div>
        <!--/right main -->
    </div>
    <!--/ main -->
<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
