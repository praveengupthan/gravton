<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gravton Motors</title>
    <?php 
        include 'includes/styles.php';
    ?>
    <?php 
        include 'includes/arrayObjects.php';
    ?>    
</head>

<body>

<?php 
    include 'includes/header.php';
?>


<!-- main -->
<main class="subMain otherpage">

<!-- container -->
<div class="container">

<!-- title row -->
<div class="row pt-2 pt-lg-5">
    <!-- col -->
    <div class="col-md-12">
        <div class="article pb-3 d-flex justify-content-between">                        
            <h2 class="h4 pb-4 text-uppercase fgreen">Why Quanta</h2> 
        </div>
    </div>
    <!-- col -->        
</div>
<!-- title row -->

<!-- row -->
<div class="row yrow">
    <!-- col -->
    <div class="col-md-6 text-center">
        <img src="img/data/yquanta01.jpg" alt="" class="yimg">
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6  align-self-center">
        <div class="article pb-3">                        
            <h2 class="h4 pb-2 text-uppercase fgreen">Strongest Battery</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem. Aliquid blanditiis deleniti, consequatur iure ullam, voluptates excepturi inventore possimus, atque pariatur sequi ducimus!</p>
        </div>
    </div>
    <!--/ col -->    
</div>
<!--/ row -->

<!-- row -->
<div class="row yrow">
    <!-- col -->
    <div class="col-md-6 text-center order-md-last">
        <img src="img/data/yquanta02.jpg" alt="" class="yimg">
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6  align-self-center">
        <div class="article pb-3">                        
            <h2 class="h4 pb-2 text-uppercase fgreen">Flexible Charging Points</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem. Aliquid blanditiis deleniti, consequatur iure ullam, voluptates excepturi inventore possimus, atque pariatur sequi ducimus!</p>
        </div>
    </div>
    <!--/ col -->    
</div>
<!--/ row -->

<!-- row -->
<div class="row yrow">
    <!-- col -->
    <div class="col-md-6 text-center">
        <img src="img/data/yquanta03.jpg" alt="" class="yimg">
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6  align-self-center">
        <div class="article pb-3">                        
            <h2 class="h4 pb-2 text-uppercase fgreen">Unbeliveable Milage</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem. Aliquid blanditiis deleniti, consequatur iure ullam, voluptates excepturi inventore possimus, atque pariatur sequi ducimus!</p>
        </div>
    </div>
    <!--/ col -->    
</div>
<!--/ row -->

<!-- row -->
<div class="row yrow">
    <!-- col -->
    <div class="col-md-6 text-center order-md-last">
        <img src="img/data/yquanta04.jpg" alt="" class="yimg">
    </div>
    <!--/ col -->

    <!-- col -->
    <div class="col-md-6  align-self-center">
        <div class="article pb-3">                        
            <h2 class="h4 pb-2 text-uppercase fgreen">Stunning Design</h2> 
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Deserunt adipisci odit tempore praesentium nisi aperiam quam atque, asperiores eveniet necessitatibus odio sequi qui sed mollitia cumque voluptas vero placeat ducimus, sapiente at quo iste doloremque quidem. Aliquid blanditiis deleniti, consequatur iure ullam, voluptates excepturi inventore possimus, atque pariatur sequi ducimus!</p>
        </div>
    </div>
    <!--/ col -->    
</div>
<!--/ row -->






</div>
<!--/ container -->
      
</main>
<!--/ main -->


<?php 
    include 'includes/footer.php';
?>

<?php 
    include 'includes/scripts.php';
?>



    
</body>
</html>
